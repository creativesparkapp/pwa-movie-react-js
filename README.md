# Bjak React movie (PWA)

Demonstration of reactJs. Migrated from ts to js from this project https://burhanhelmy@bitbucket.org/creativesparkapp/pwa-movie-react.git

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

A step by step series of examples that tell you how to get a development env running

clone repo

```
git clone https://burhanhelmy@bitbucket.org/creativesparkapp/pwa-movie-react-js.git
```

go to working folder and install dependencies

```
npm install
```

to run project the project,in working folder run

```
npm start
```

Open http://localhost:3000 to view it in the browser.

DEMO: https://bjak-movies.web.app/ (deployed in firebase , using previous demo link (typescript codebase) because it is similar)

NOTE: To test the pwa features, you can add the page at homescreen on safari mobile. (Tested on iphone XR)


## Built With

* [Create React App](https://github.com/facebook/create-react-app) - js library to build UI
* [axios](https://github.com/axios/axios) - http
* [reactstrap](https://reactstrap.github.io/) - UI framework
* [Argon Design System React](https://maven.apache.org/) - design system styling


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details