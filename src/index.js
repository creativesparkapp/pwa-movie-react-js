import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './assets/css/blk-design-system-react.css';
import './assets/css/nucleo-icons.css';
import * as serviceWorker from './serviceWorker';
import createHistory from 'history/createBrowserHistory'
import { Router } from 'react-router-dom';

import Root from './views/root/root';

class App extends React.Component {
    history = createHistory();
    render() {
        return (
            <Router history={this.history}>
                <Root> </Root>
            </Router>
        )
    }
}

ReactDOM.render(<App />,
    document.getElementById('root'));

serviceWorker.register();
